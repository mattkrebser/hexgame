﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace HexGame
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Form1 f;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(f=new Form1());
        }
    }
}
