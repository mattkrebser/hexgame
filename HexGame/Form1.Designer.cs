﻿namespace HexGame
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NextMove = new System.Windows.Forms.Button();
            this.StartPlayer1 = new System.Windows.Forms.Button();
            this.StartPlayer2 = new System.Windows.Forms.Button();
            this.Winner = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.P1Select = new System.Windows.Forms.ComboBox();
            this.P2Select = new System.Windows.Forms.ComboBox();
            this.CanMove = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // NextMove
            // 
            this.NextMove.Location = new System.Drawing.Point(394, 469);
            this.NextMove.Name = "NextMove";
            this.NextMove.Size = new System.Drawing.Size(75, 23);
            this.NextMove.TabIndex = 0;
            this.NextMove.Text = "Next Move";
            this.NextMove.UseVisualStyleBackColor = true;
            this.NextMove.Click += new System.EventHandler(this.NextMove_Click);
            // 
            // StartPlayer1
            // 
            this.StartPlayer1.Location = new System.Drawing.Point(414, 14);
            this.StartPlayer1.Name = "StartPlayer1";
            this.StartPlayer1.Size = new System.Drawing.Size(55, 23);
            this.StartPlayer1.TabIndex = 1;
            this.StartPlayer1.Text = "Player1";
            this.StartPlayer1.UseVisualStyleBackColor = true;
            this.StartPlayer1.Click += new System.EventHandler(this.StartPlayer1_Click);
            // 
            // StartPlayer2
            // 
            this.StartPlayer2.Location = new System.Drawing.Point(414, 43);
            this.StartPlayer2.Name = "StartPlayer2";
            this.StartPlayer2.Size = new System.Drawing.Size(55, 23);
            this.StartPlayer2.TabIndex = 2;
            this.StartPlayer2.Text = "Player2";
            this.StartPlayer2.UseVisualStyleBackColor = true;
            this.StartPlayer2.Click += new System.EventHandler(this.StartPlayer2_Click);
            // 
            // Winner
            // 
            this.Winner.Location = new System.Drawing.Point(12, 469);
            this.Winner.Name = "Winner";
            this.Winner.Size = new System.Drawing.Size(237, 23);
            this.Winner.TabIndex = 3;
            this.Winner.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 450);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Player1=Blue,Player2=Green";
            // 
            // P1Select
            // 
            this.P1Select.FormattingEnabled = true;
            this.P1Select.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.P1Select.Location = new System.Drawing.Point(255, 471);
            this.P1Select.Name = "P1Select";
            this.P1Select.Size = new System.Drawing.Size(46, 21);
            this.P1Select.TabIndex = 5;
            this.P1Select.SelectedIndexChanged += new System.EventHandler(this.P1Select_SelectedIndexChanged);
            // 
            // P2Select
            // 
            this.P2Select.FormattingEnabled = true;
            this.P2Select.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.P2Select.Location = new System.Drawing.Point(307, 471);
            this.P2Select.Name = "P2Select";
            this.P2Select.Size = new System.Drawing.Size(46, 21);
            this.P2Select.TabIndex = 6;
            this.P2Select.SelectedIndexChanged += new System.EventHandler(this.P2Select_SelectedIndexChanged);
            // 
            // CanMove
            // 
            this.CanMove.AutoSize = true;
            this.CanMove.ForeColor = System.Drawing.Color.Red;
            this.CanMove.Location = new System.Drawing.Point(255, 449);
            this.CanMove.Name = "CanMove";
            this.CanMove.Size = new System.Drawing.Size(97, 13);
            this.CanMove.TabIndex = 7;
            this.CanMove.Text = "Cannot Move Here";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(481, 504);
            this.Controls.Add(this.CanMove);
            this.Controls.Add(this.P2Select);
            this.Controls.Add(this.P1Select);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Winner);
            this.Controls.Add(this.StartPlayer2);
            this.Controls.Add(this.StartPlayer1);
            this.Controls.Add(this.NextMove);
            this.Name = "Form1";
            this.Text = "Hexagon Nonsense";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button NextMove;
        private System.Windows.Forms.Button StartPlayer1;
        private System.Windows.Forms.Button StartPlayer2;
        private System.Windows.Forms.Button Winner;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox P1Select;
        private System.Windows.Forms.ComboBox P2Select;
        private System.Windows.Forms.Label CanMove;
    }
}

