﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace HexGame
{
    public class HexPlayer
    {
        List<PointPair> AllLines;
        HexNode GameState;
        int[] Points;

        //4 look aheads 0,1,2,3,4 -0 is root
        const int DepthLimit = 4;

        public HexPlayer()
        {
            Points = new int[] { 0, 1, 2, 3, 4, 5 };
            //compute all lines
            HashSet<PointPair> ps = new HashSet<PointPair>();
            foreach (var p1 in Points)
            {
                foreach (var p2 in Points)
                {
                    if (p1.Equals(p2)) continue;
                    PointPair pp = new PointPair(p1, p2);
                    if (!ps.Contains(pp))
                        ps.Add(pp);
                }
            }
            AllLines = new List<PointPair>(ps);
            GameState = new HexNode();
        }

        /// <summary>
        /// Game over?
        /// </summary>
        public Player Winner
        {
            get
            {
                return GameState.Winner();
            }
        }

        /// <summary>
        /// Can add the input line to the game?
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool CanMove(int v1, int v2)
        {
            PointPair p = new PointPair(v1, v2);
            return !GameState.HasEdge(p);
        }

        /// <summary>
        /// Compute the best next move for the input player. This does not change the game state
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public PointPair NextMove(Player player)
        {
            if (GameState.GameOver)
            {
                return default(PointPair);
            }

            HexNode next = GameState.MinMax(AllLines, player);
            return next.Transition;
        }

        /// <summary>
        /// Manually Set the input players next move
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public void NextMove(Player player, int v1, int v2)
        {
            if (GameState.GameOver)
            {
                return;
            }

            PointPair p = new PointPair(v1, v2);
            if (GameState.HasEdge(p))
                throw new System.Exception("Error, cannot move here");
            GameState = GameState.Next(p, player);
        }

        /// <summary>
        /// Possible gamestate
        /// </summary>
        class HexNode
        {
            /// <summary>
            /// GameState of this node
            /// </summary>
            Dictionary<PointPair, Player> GameState = new Dictionary<PointPair, Player>();
            /// <summary>
            /// Copy of this nodes game state
            /// </summary>
            Dictionary<PointPair, Player> CopyState
            {
                get
                {
                    Dictionary<PointPair, Player> newp = new Dictionary<PointPair, Player>();
                    foreach (var v in GameState)
                    {
                        newp.Add(v.Key, v.Value);
                    }
                    return newp;
                }
            }

            /// <summary>
            /// Transition used to get to this state from the parent
            /// </summary>
            public PointPair Transition;

            /// <summary>
            /// Sum of children branches
            /// </summary>
            int sum = 0;

            /// <summary>
            /// Is this an end state?
            /// </summary>
            bool gameOver = false;

            /// <summary>
            /// Game Over?
            /// </summary>
            public bool GameOver
            {
                get
                {
                    return gameOver;
                }
            }

            /// <summary>
            /// Depth of this node
            /// </summary>
            int Depth = 0;
            /// <summary>
            /// Depth of deepest child
            /// </summary>
            int MaxDepth = 0;

            /// <summary>
            /// Compute children of this node.
            /// </summary>
            /// <returns></returns>
            public HexNode MinMax(IEnumerable<PointPair> AllChoices, Player MyMove)
            {
                //winner?
                var winner = Winner();
                //no winner, get sum of children
                if (winner == Player.NONE)
                {
                    //if we have exceeded the depth limit
                    //(cannot compute entire game, it takes too long,
                    //15! possible states
                    if (Depth >= DepthLimit)
                    {
                        //so just return that this state will not result in a loss
                        sum = (int)Player.NONE;
                        MaxDepth = Depth;
                        return this;
                    }

                    List<HexNode> children = new List<HexNode>();
                    foreach (var p in AllChoices)
                    {
                        if (!GameState.ContainsKey(p))
                        {
                            HexNode child = new HexNode();
                            child.GameState = CopyState;
                            child.GameState.Add(new PointPair(p.v1, p.v2), MyMove);
                            child.Transition = new PointPair(p.v1, p.v2);
                            child.Depth = Depth + 1;
                            children.Add(child);
                            //sum children
                            child.MinMax(AllChoices, OtherPlayer(MyMove));
                        }
                    }

                    HexNode Next = null;
                    //maximize
                    if (MyMove == Player.ONE)
                    {
                        sum = int.MinValue;
                        //get max
                        foreach (var child in children)
                        {
                            Next = child;
                            sum = child.sum;
                        }
                    }
                    //minimize
                    else
                    {
                        sum = int.MaxValue;
                        //get min
                        foreach (var child in children)
                        {
                            Next = child;
                            sum = child.sum;
                        }
                    }

                    //assign max depth
                    foreach (var child in children)
                        if (child.MaxDepth > MaxDepth)
                            MaxDepth = child.MaxDepth;
                    //if this is the root node and we are picking a edge that will make us loose...
                    if (Depth==0)
                    {
                        //ensure that we pick the edge with the greatest depth,
                        //at this point we are guarentted to loose, but need the game to go on as long as possible
                        //this can also be viewed as a sorter between ties, (nodes of equal weights)
                        if (MyMove == Player.ONE && sum < 0 || MyMove == Player.TWO && sum > 0)
                        {
                            foreach (var child in children)
                                if (child.MaxDepth == MaxDepth)
                                    return child;
                        }
                    }

                    return Next;
                }
                //get sum of winner (+1 or -1)
                else
                {
                    gameOver = true;
                    sum = (int)winner;
                    MaxDepth = Depth;
                    return this;
                }
            }

            /// <summary>
            /// Get opposite player
            /// </summary>
            /// <param name="p"></param>
            /// <returns></returns>
            public Player OtherPlayer(Player p)
            {
                return p == Player.ONE ? Player.TWO : Player.ONE;
            }

            /// <summary>
            /// Return a node that is this node + input transition
            /// </summary>
            /// <param name="p"></param>
            /// <param name="player"></param>
            /// <returns></returns>
            public HexNode Next(PointPair p, Player player)
            {
                HexNode next = new HexNode();
                next.GameState = CopyState;
                next.Transition = new PointPair(p.v1, p.v2);
                next.Add(next.Transition, player);
                return next;
            }

            public bool HasEdge(PointPair p)
            {
                if (GameState == null)
                    return false;
                return GameState.ContainsKey(p);
            }

            public void Add(PointPair p, Player pl)
            {
                GameState.Add(p, pl);
            }

            /// <summary>
            /// Winner?
            /// </summary>
            /// <param name="player"></param>
            /// <returns></returns>
            public Player Winner()
            {
                if (GameState == null || GameState.Count <= 2)
                    return Player.NONE;
                foreach (var edge1 in GameState)
                {
                    foreach (var edge2 in GameState)
                    {
                        if (!edge2.Equals(edge1))
                        {
                            //if shared vertice
                            if (edge1.Key.v1 == edge2.Key.v1)
                            {
                                //if contains triangle...
                                if (GameState.ContainsKey(new PointPair(edge1.Key.v2, edge2.Key.v2)))
                                {
                                    if (edge1.Value == edge2.Value &&
                                        edge1.Value == GameState[new PointPair(edge1.Key.v2, edge2.Key.v2)])
                                        return OtherPlayer(edge1.Value);
                                }
                            }
                            else if (edge1.Key.v2 == edge2.Key.v2)
                            {
                                //if contains triangle...
                                if (GameState.ContainsKey(new PointPair(edge1.Key.v1, edge2.Key.v1)))
                                {
                                    if (edge1.Value == edge2.Value &&
                                        edge1.Value == GameState[new PointPair(edge1.Key.v1, edge2.Key.v1)])
                                        return OtherPlayer(edge1.Value);
                                }
                            }
                            else if (edge1.Key.v1 == edge2.Key.v2)
                            {
                                //if contains triangle...
                                if (GameState.ContainsKey(new PointPair(edge1.Key.v2, edge2.Key.v1)))
                                {
                                    if (edge1.Value == edge2.Value &&
                                        edge1.Value == GameState[new PointPair(edge1.Key.v2, edge2.Key.v1)])
                                        return OtherPlayer(edge1.Value);
                                }
                            }
                            else if (edge1.Key.v2 == edge2.Key.v1)
                            {
                                //if contains triangle...
                                if (GameState.ContainsKey(new PointPair(edge1.Key.v1, edge2.Key.v2)))
                                {
                                    if (edge1.Value == edge2.Value &&
                                        edge1.Value == GameState[new PointPair(edge1.Key.v1, edge2.Key.v2)])
                                        return OtherPlayer(edge1.Value);
                                }
                            }
                            //else disjoint
                        }
                    }
                }
                return Player.NONE;
            }
        }
    }

    /// <summary>
    /// Represnrting a line between two points
    /// </summary>
    public struct PointPair : IEquatable<PointPair>
    {
        public int v1
        {
            get
            {
                return _v1;
            }
        }
        public int v2
        {
            get
            {
                return _v2;
            }
        }

        int _v1; int _v2;

        public PointPair(int a, int b)
        {
            if (a == b) throw new System.Exception("undefined point");
            if (a < b)
            {
                _v1 = a; _v2 = b;
            }
            else
            {
                _v1 = b; _v2 = a;
            }
        }

        /// <summary>
        /// return true if undefined line
        /// </summary>
        public bool Undefined
        {
            get
            {
                return v1 == v2;
            }
        }

        public bool Equals(PointPair p)
        {
            return (p.v1 == v1 && p.v2 == v2);
        }
        public override bool Equals(object obj)
        {
            return obj is PointPair ? Equals((PointPair)obj) : false;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 29 + v1.GetHashCode();
                hash = hash * 29 + v2.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return "(" + v1.ToString() + ", " + v2.ToString() + ")";
        }
    }

    /// <summary>
    /// Player one or two
    /// </summary>
    public enum Player
    {
        /// <summary>
        /// player one maximizes
        /// </summary>
        ONE = 1,
        /// <summary>
        /// player two minimizes
        /// </summary>
        TWO = -1,
        NONE = 0
    }
}
