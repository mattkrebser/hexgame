﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

namespace HexGame
{
    public partial class Form1 : Form
    {

        System.Drawing.Graphics graphics;
        Point[] Points = new Point[6];
        HexPlayer game;

        Player CurrentPlayer;

        Player UserPlayer;

        public Form1()
        {
            InitializeComponent();
            graphics = this.CreateGraphics();
            this.Paint += Initialize;
        }

        void Initialize(object sender, PaintEventArgs e)
        {
            SolidBrush b = new SolidBrush(Color.Black);
            SolidBrush rb = new SolidBrush(Color.Red);
            System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(
            10, 10, 400, 400);
            graphics.DrawRectangle(System.Drawing.Pens.Black, rectangle);

            var r = rectangle.Width * 0.4f;
            var r2 = rectangle.Width * 0.04f + 1;
            int x_0 = rectangle.Left + rectangle.Width / 2;
            int y_0 = rectangle.Bottom - rectangle.Height / 2;
            Font f = new Font(FontFamily.GenericSerif, 13);
            //Create 6 points, by iterating 260 degrees around a point (a circle)
            for (int a = 0; a < 6; a++)
            {
                var point = new PointF(
                    x_0 + r * (float)Math.Cos(a * 60 * Math.PI / 180f),
                    y_0 + r * (float)Math.Sin(a * 60 * Math.PI / 180f));
                float x = point.X - r2;
                float y = point.Y - r2;
                graphics.FillEllipse(b, new Rectangle((int)x, (int)y, (int)r2, (int)r2));
                Points[a] = new Point((int)(point.X - r2/2), (int)(point.Y - r2/2));
                graphics.DrawString(a.ToString(), f, rb, new Point((int)(point.X-r2), (int)(point.Y-r2)));
            }
            this.Paint -= Initialize;
            game = new HexPlayer();
        }

        /// <summary>
        /// Draw a line between vertices
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="c"></param>
        public void Line(int v1, int v2, Color c)
        {
            Pen p = new Pen(c);
            PointF p1 = Points[v1];
            PointF p2 = Points[v2];

            p1 = Add(p1, Mult(Dir(p1, p2), 10));
            p2 = Add(p2, Mult(Dir(p2, p1), 10));
            graphics.DrawLine(p, p1, p2);
        }
        /// <summary>
        /// normalized direction of a line
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        PointF Dir(PointF p1, PointF p2)
        {
            PointF d = new PointF(p2.X - p1.X, p2.Y - p1.Y);
            d = new PointF(d.X / Magnitude(d), d.Y / Magnitude(d));
            return new PointF(d.X, d.Y);
        }
        /// <summary>
        /// add points
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        PointF Add(PointF p1, PointF p2)
        {
            return new PointF(p1.X + p2.X, p1.Y + p2.Y);
        }
        /// <summary>
        /// scalar multiply
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="f"></param>
        /// <returns></returns>
        PointF Mult(PointF p1, float f)
        {
            return new PointF(p1.X * f, p1.Y * f);
        }
        /// <summary>
        /// magnitude of a vector
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        float Magnitude(PointF p)
        {
            return (float)Math.Sqrt(p.X * p.X + p.Y * p.Y);
        }

        /// <summary>
        /// Advance the game one move.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextMove_Click(object sender, EventArgs e)
        {
            if (StartPlayer1.Enabled) return;

            //game over GG
            if (game.Winner != Player.NONE)
            {
                return;
            }

            PointPair p;
            //if current player is user
            if (CurrentPlayer==UserPlayer)
            {
                P1Select_SelectedIndexChanged(null, null);
                P2Select_SelectedIndexChanged(null, null);
                //do validation check (make sure the user inputs a valid move)
                if (!string.IsNullOrEmpty(CanMove.Text))
                    return;
                p = new PointPair(P1Select.SelectedIndex, P2Select.SelectedIndex);
                game.NextMove(CurrentPlayer, p.v1, p.v2);
            }
            else
            {
                p = game.NextMove(CurrentPlayer);
                game.NextMove(CurrentPlayer, p.v1, p.v2);
            }

            //advance
            Line(p.v1, p.v2, CurrentPlayer == Player.ONE ? Color.Blue : Color.YellowGreen);
            //switch players
            CurrentPlayer = CurrentPlayer == Player.ONE ? Player.TWO : Player.ONE;

            //game over?
            if (game.Winner != Player.NONE)
            {
                Winner.Text = "Player " + game.Winner.ToString() + " Wins!";
            }
        }

        private void StartPlayer2_Click(object sender, EventArgs e)
        {
            CurrentPlayer = Player.ONE;
            UserPlayer = Player.TWO;
            StartPlayer1.Enabled = false;
            StartPlayer2.Enabled = false;
            StartPlayer1.Visible = false;
            StartPlayer2.Visible = false;
        }

        private void StartPlayer1_Click(object sender, EventArgs e)
        {
            CurrentPlayer = Player.ONE;
            UserPlayer = Player.ONE;
            StartPlayer1.Enabled = false;
            StartPlayer2.Enabled = false;
            StartPlayer1.Visible = false;
            StartPlayer2.Visible = false;
        }

        private void P1Select_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (game==null || P1Select.SelectedIndex==P2Select.SelectedIndex ||
                P2Select.SelectedIndex < 0 || P1Select.SelectedIndex < 0)
            {
                CanMove.Text = "Cannot Move Here";
                return;
            }
            if (game.CanMove(P1Select.SelectedIndex, P2Select.SelectedIndex))
            {
                CanMove.Text = "";
            }
            else
            {
                CanMove.Text = "Cannot Move Here";
            }
        }

        private void P2Select_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (game == null || P1Select.SelectedIndex == P2Select.SelectedIndex ||
                P2Select.SelectedIndex < 0 || P1Select.SelectedIndex < 0)
            {
                CanMove.Text = "Cannot Move Here";
                return;
            }
            if (game.CanMove(P1Select.SelectedIndex, P2Select.SelectedIndex))
            {
                CanMove.Text = "";
            }
            else
            {
                CanMove.Text = "Cannot Move Here";
            }
        }
    }
}
