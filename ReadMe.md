**HexGame**
*A Game where the first player to construct a triangle loses.
*The Game is implemented using a basic MINMAX Search tree with a variable look ahead(default of 4)


**USE:**
navigate to: HexGame\HexGame\bin\Debug
Use windows command prompt to run the program or double click it

-Example: > cd HexGame\HexGame\bin\Debug
-	 > HexGame.exe

-HexGame.exe is the name of the program

-To start the game press 'Player1' or 'Player2'
-Player1 always moves first. Selecting 'Player1' will make the user play as   player1

There are two drop down menus to select the line you wish to draw.
A red message will tell you if the line is invalid.

Press the 'Next Move' button to input your move. You must also press 'Next Move' to
compute the computer's next move.

The game will continue until one player draws a triangle.

This project is a visual studio 2015 (community edition) windows form application
You will need .NET runtime environment to run the application

To compile the program, you will need visual studio. (2015 would be best, .NET version 4.6)
just open the project file in visual studio and press the build button.

![exampleimage.png](https://bitbucket.org/repo/nzKXdz/images/1252135806-exampleimage.png)